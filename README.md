# Overview 
RESTful Api for money transfers between accounts

## Libraries Used:
* com.sparkjava:spark-core:2.3
* com.h2database:h2:1.4.196
* com.google.code.gson:gson:2.8.0
* org.slf4j:slf4j-log4j12:1.7.25
* com.typesafe:config:1.3.2
* junit:junit:4.12
* io.rest-assured:rest-assured:3.3.0

## Usage

You can use docker to run the project or build it from source.

### Docker:
    sudo docker run -p 8080:8080 ziadm/accounts-service
    
### From Source:
    git clone https://github.com/ziadmoubayed/revolut-backend.git
    mvn clean package
    java -cp target/accounts-service-0.0.1-SNAPSHOT-jar-with-dependencies.jar com.revolut.test.accounts.AccountsApp
    
## End Points

### Accounts:
    GET /accounts
    GET /accounts/:id
    POST /accounts

### Transfers:
    GET /transfers
    GET /trsnfers/:id
    POST /transfers

## Limitations

* Using an embedded h2 db which is not good at handling concurrency, thus connections are limited to 1.
