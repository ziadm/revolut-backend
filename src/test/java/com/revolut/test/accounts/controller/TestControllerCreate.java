package com.revolut.test.accounts.controller;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;

import java.math.BigDecimal;

import com.revolut.test.accounts.util.RestContext;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.gson.GsonBuilder;
import com.revolut.test.accounts.models.Account;
import com.revolut.test.accounts.repository.impl.H2AccountsRepo;
import com.revolut.test.accounts.service.impl.AccountServiceImpl;

import io.restassured.http.ContentType;

public class TestControllerCreate {

    private static RestContext testServer;

    @BeforeClass
    public static void init() {
        testServer = new RestContext(8080, "");
        testServer.addEndpoint(new AccountsController(new AccountServiceImpl(new H2AccountsRepo()), new GsonBuilder().create()));
        testServer.awaitInit();
    }

    @AfterClass
    public static void destroy() {
        testServer.stop();
    }

    @Test
    public void shouldCreateAccount() {
        Account reqAcc = new Account(1L, "number", BigDecimal.valueOf(50d));
        given().contentType(ContentType.JSON)
                .body(reqAcc).when()
                .post("/accounts")
                .then()
                .statusCode(201)
                .body(
                        "result", notNullValue(),
                        "code", is(0),
                        "message", isEmptyOrNullString()
                );
    }

    @Test
    public void shouldFailIfAccountNumberDoesNotExist() {
        Account reqAcc = new Account(null, BigDecimal.valueOf(50d));
        given()
                .contentType(ContentType.JSON)
                .body(reqAcc)
                .when()
                .post("/accounts")
                .then()
                .statusCode(400)
                .body("result", nullValue(),
                        "code", is(-1),
                        "message", is("Account number does not exist"));

    }

    @Test
    public void shouldFailIfAccountBalanceDoesNotExist() {
        Account reqAcc = new Account("somenumber", null);
        given()
                .contentType(ContentType.JSON)
                .body(reqAcc)
                .when()
                .post("/accounts")
                .then()
                .statusCode(400)
                .body("result", nullValue(),
                        "code", is(-1),
                        "message", is("Account balance does not exist"));

    }
}
