package com.revolut.test.accounts.controller;

import com.google.gson.GsonBuilder;
import com.revolut.test.accounts.models.Account;
import com.revolut.test.accounts.repository.impl.H2AccountsRepo;
import com.revolut.test.accounts.service.AccountService;
import com.revolut.test.accounts.service.impl.AccountServiceImpl;
import com.revolut.test.accounts.util.RestContext;
import io.restassured.http.ContentType;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertEquals;

public class TestControllerRead {

    private static RestContext testServer;

    @BeforeClass
    public static void init() throws Exception {
        AccountService service = new AccountServiceImpl(new H2AccountsRepo());
        service.createNewAccount(new Account(1L, "number1", BigDecimal.valueOf(50d)));
        service.createNewAccount(new Account(2L, "number2", BigDecimal.valueOf(60d)));
        service.createNewAccount(new Account(3L, "number3", BigDecimal.valueOf(70d)));

        testServer = new RestContext(8080, "");
        testServer.addEndpoint(new AccountsController(service, new GsonBuilder().create()));
        testServer.awaitInit();
    }

    @AfterClass
    public static void destroy() {
        testServer.stop();
    }

    @Test
    public void shouldReturnAListOfAccounts() {
        given().when().get("/accounts")
                .then().statusCode(200)
                .body(
                        "result", notNullValue(),
                        "code", is(0),
                        "message", isEmptyOrNullString()
                );
    }

    @Test
    public void shouldReturnNotFoundError() {
        given().when().get("/accounts/-1").then().statusCode(404)
                .body(
                        "result", nullValue(),
                        "code", is(-1),
                        "message", is("Account [-1] not found")
                );
    }

    @Test
    public void validateSavingResponseWorks() throws Exception {
        AccountService service = new AccountServiceImpl(new H2AccountsRepo());
        Account account = service.createNewAccount(new Account("number1", BigDecimal.valueOf(50d)));
        Account response = given().body(account).contentType(ContentType.JSON)
                .when().post("/accounts").then()
                .statusCode(201).body(
                        "code", is(0),
                        "message", isEmptyOrNullString()
                ).extract().jsonPath().getObject("result", Account.class);

        assertEquals(account.getNumber(), response.getNumber());
        assertEquals(0, account.getBalance().compareTo(response.getBalance()));

        Account getResponse = given()
                .when().get("/accounts/" + response.getId()).then().statusCode(200)
                .body(
                        "code", is(0),
                        "message", isEmptyOrNullString()
                ).extract().jsonPath().getObject("result", Account.class);

        assertEquals(account.getNumber(), getResponse.getNumber());
        assertEquals(0, account.getBalance().compareTo(getResponse.getBalance()));
    }
}
