package com.revolut.test.accounts.service;

import com.revolut.test.accounts.controller.exception.InvalidPayloadException;
import com.revolut.test.accounts.controller.exception.NotFoundException;
import com.revolut.test.accounts.models.Account;
import com.revolut.test.accounts.repository.impl.H2AccountsRepo;
import com.revolut.test.accounts.service.impl.AccountServiceImpl;
import org.junit.*;
import org.junit.runners.MethodSorters;

import java.math.BigDecimal;
import java.util.Collection;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestAccountsService {
    private static AccountService service;

    @BeforeClass
    public static void setUp() throws Exception {
        service = new AccountServiceImpl(new H2AccountsRepo());
    }

    @Test
    public void savingAnAccountShouldPersistInDatabase() throws Exception {
        Account account1 = service.createNewAccount(new Account("2018-001-001", BigDecimal.valueOf(10000)));
        Assert.assertNotNull(account1);
        Assert.assertEquals(account1.getId(), service.getAccount(account1.getId()).getId());
    }

    @Test(expected = InvalidPayloadException.class)
    public void savingAccountWithoutNumberShouldFail() throws Exception {
        Account account = service.createNewAccount(new Account(null, BigDecimal.valueOf(10000)));
    }

    @Test(expected = InvalidPayloadException.class)
    public void savingAccountWithoutBalanceShouldFail() throws Exception {
        Account account = service.createNewAccount(new Account("somenumber", null));
    }

    @Test(expected = NotFoundException.class)
    public void wrongIdShouldThrowNotFoundException() throws Exception {
    	service.getAccount(-1L);
    }

    @Test(expected = InvalidPayloadException.class)
    public void nullAccountShouldThrowInvalidExecption() throws Exception {
        Account entity = service.createNewAccount(null);
    }
}
