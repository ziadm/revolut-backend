package com.revolut.test.transfers.service;

import com.revolut.test.accounts.controller.exception.NotFoundException;
import com.revolut.test.accounts.controller.exception.TransferRejectedException;
import com.revolut.test.accounts.models.Account;
import com.revolut.test.accounts.models.TransferRequest;
import com.revolut.test.accounts.repository.impl.H2AccountsRepo;
import com.revolut.test.accounts.repository.impl.H2TransfersRepo;
import com.revolut.test.accounts.service.AccountService;
import com.revolut.test.accounts.service.TransferService;
import com.revolut.test.accounts.service.impl.AccountServiceImpl;
import com.revolut.test.accounts.service.impl.TransferServiceImpl;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertTrue;

public class TestTransferService {

    private static TransferService transferService;
    private static AccountService accountService;

    @BeforeClass
    public static void init() {
        accountService = new AccountServiceImpl(new H2AccountsRepo());
        transferService = new TransferServiceImpl(new H2TransfersRepo(), new H2AccountsRepo());
    }

    @Test(expected = NotFoundException.class)
    public void shouldThrowNotFoundForWrongId() throws Exception {
        transferService.getTransfer(-50L);
    }

    @Test(expected = TransferRejectedException.class)
    public void shouldFailIfAmmountGreaterThanBalance() throws Exception {
        Account account1 = accountService.createNewAccount(new Account("2018-001-001", new BigDecimal(10000)));
        Account account2 = accountService.createNewAccount(new Account("2018-001-002", new BigDecimal(15000)));

        transferService.transfer(new TransferRequest(account1.getId(), account2.getId(), BigDecimal.valueOf(5000000)));
    }

    @Test(expected = NotFoundException.class)
    public void shouldfailNotFoundForWrongIdInTransfer() throws Exception {
        transferService.transfer(new TransferRequest(-1L, 2L, BigDecimal.valueOf(5000000)));
    }

    @Test()
    public void goodTransferShouldChangeBalances() throws Exception {
        Account account3 = accountService.createNewAccount(new Account("2018-001-001", new BigDecimal(10000)));
        Account account4 = accountService.createNewAccount(new Account("2018-001-002", new BigDecimal(15000)));

        transferService.transfer(new TransferRequest(account3.getId(), account4.getId(), BigDecimal.valueOf(50)));
        assertTrue(account3.getBalance().subtract(accountService.getAccount(account3.getId()).getBalance()).compareTo(new BigDecimal(50)) == 0);
        assertTrue(accountService.getAccount(account4.getId()).getBalance().subtract(account4.getBalance()).compareTo(new BigDecimal(50)) == 0);
    }
}
