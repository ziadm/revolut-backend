package com.revolut.test.transfers.service;

import com.revolut.test.accounts.models.Account;
import com.revolut.test.accounts.models.TransferRequest;
import com.revolut.test.accounts.repository.impl.H2AccountsRepo;
import com.revolut.test.accounts.repository.impl.H2TransfersRepo;
import com.revolut.test.accounts.service.AccountService;
import com.revolut.test.accounts.service.TransferService;
import com.revolut.test.accounts.service.impl.AccountServiceImpl;
import com.revolut.test.accounts.service.impl.TransferServiceImpl;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;

public class TestTransferConsistency {

    @Test
    public void concurrencyTest() throws Exception {
        AccountService accountService = new AccountServiceImpl(new H2AccountsRepo());
        TransferService transferService = new TransferServiceImpl(new H2TransfersRepo(), new H2AccountsRepo());
        System.out.println("Started testing concurrency");

        Account account1 = accountService.createNewAccount(new Account("2018-001-001", new BigDecimal(100000000)));
        Account account2 = accountService.createNewAccount(new Account("2018-001-002", new BigDecimal(150000000)));
        System.out.println(accountService.getAllAccounts());

        final int count = 500;
        ExecutorService executor = Executors.newFixedThreadPool(3);

        CountDownLatch latch = new CountDownLatch(count * 2);
        IntStream.rangeClosed(1, count)
                .parallel()
                .forEach(i -> {
                    executor.submit(() -> {
                        try {
                            transferService.transfer(new TransferRequest(account1.getId(), account2.getId(), BigDecimal.valueOf(i)));
                            latch.countDown();
                        } catch (Exception e) {
                            System.err.println(e.getMessage());
                        }
                    });
                });
        IntStream.rangeClosed(1, count)
                .parallel()
                .forEach(i -> {
                    executor.submit(() -> {
                        try {
                            transferService.transfer(new TransferRequest(account2.getId(), account1.getId(), BigDecimal.valueOf(i)));
                            latch.countDown();
                        } catch (Exception e) {
                            System.err.println(e.getMessage());
                        }
                    });
                });
        latch.await();
        int sum = IntStream.rangeClosed(1, count).sum();
        System.out.println("Sum: " + sum);
        BigDecimal newBalance1 = accountService.getAccount(account1.getId()).getBalance();
        BigDecimal newBalance2 = accountService.getAccount(account2.getId()).getBalance();
        System.out.println(newBalance1);
        System.out.println(newBalance2);

        System.out.println(transferService.getAllTransfers().size());
        assertEquals(0, account1.getBalance().subtract(newBalance1).compareTo(new BigDecimal(0)));
        assertEquals(0, newBalance2.subtract(account2.getBalance()).compareTo(new BigDecimal(0)));
    }
}
