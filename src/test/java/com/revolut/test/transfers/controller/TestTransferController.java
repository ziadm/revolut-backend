package com.revolut.test.transfers.controller;

import com.google.gson.GsonBuilder;
import com.revolut.test.accounts.controller.TransferController;
import com.revolut.test.accounts.repository.impl.H2AccountsRepo;
import com.revolut.test.accounts.repository.impl.H2TransfersRepo;
import com.revolut.test.accounts.service.impl.TransferServiceImpl;
import com.revolut.test.accounts.util.RestContext;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;

public class TestTransferController {

    private static RestContext testServer;

    @BeforeClass
    public static void init() {
        testServer = new RestContext(8080, "");
        testServer.addEndpoint(new TransferController(new TransferServiceImpl(new H2TransfersRepo(), new H2AccountsRepo()),
                new GsonBuilder().create()));
        testServer.awaitInit();
    }

    @AfterClass
    public static void destroy() {
        testServer.stop();
    }

    @Test
    public void shouldReturnAListOfTransfers() {
        given().when().get("/transfers")
                .then().statusCode(200)
                .body(
                        "result", notNullValue(),
                        "code", is(0),
                        "message", isEmptyOrNullString()
                );
    }

    @Test
    public void shouldReturnNotFoundError() {
        given().when().get("/transfers/-1").then().statusCode(404)
                .body(
                        "result", nullValue(),
                        "code", is(-1),
                        "message", is("Transfer [-1] not found")
                );
    }


}
