package com.revolut.test;

import com.google.gson.GsonBuilder;
import com.revolut.test.accounts.controller.AccountsController;
import com.revolut.test.accounts.controller.TransferController;
import com.revolut.test.accounts.models.Account;
import com.revolut.test.accounts.repository.impl.H2AccountsRepo;
import com.revolut.test.accounts.repository.impl.H2TransfersRepo;
import com.revolut.test.accounts.service.AccountService;
import com.revolut.test.accounts.service.impl.AccountServiceImpl;
import com.revolut.test.accounts.service.impl.TransferServiceImpl;
import com.revolut.test.accounts.util.RestContext;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;

import static io.restassured.RestAssured.given;

public class TestBaseController {

    private static RestContext testServer;

    @BeforeClass
    public static void init() {
        testServer = new RestContext(8080, "");

        AccountService service = new AccountServiceImpl(new H2AccountsRepo());
        testServer.addEndpoint(new AccountsController(service, new GsonBuilder().create()));
        testServer.awaitInit();
    }

    @AfterClass
    public static void destroy() {
        testServer.stop();
    }

    @Test
    public void testContentTypeValidation() {
        Account reqAcc = new Account("number", BigDecimal.valueOf(50));
        given().body(reqAcc)
                .when().post("/accounts").then()
                .statusCode(400);
    }
}
