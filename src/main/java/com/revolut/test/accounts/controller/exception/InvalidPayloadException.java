package com.revolut.test.accounts.controller.exception;

@SuppressWarnings("serial")
public class InvalidPayloadException extends Exception {

    public InvalidPayloadException() {
        super();
    }

    public InvalidPayloadException(String message) {
        super(message);
    }

}