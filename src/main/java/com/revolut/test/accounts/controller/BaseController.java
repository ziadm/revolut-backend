package com.revolut.test.accounts.controller;

import com.google.gson.Gson;
import com.revolut.test.accounts.controller.exception.InvalidPayloadException;
import com.revolut.test.accounts.controller.exception.NotFoundException;
import com.revolut.test.accounts.controller.exception.TransferRejectedException;
import com.revolut.test.accounts.models.Response;
import com.revolut.test.accounts.util.Constants;
import org.eclipse.jetty.http.HttpStatus;
import spark.Request;
import spark.ResponseTransformer;

import static spark.Spark.*;

public abstract class BaseController {

    private final ResponseTransformer responseTransormer;

    public BaseController(final Gson jsonConverter) {

        // -- validate request
        before((req, res) -> {
            //TODO add authentication
        });

        // -- Set proper content-type to all responses
        after((req, res) -> {
            res.type(Constants.STANDARD_RESPONSE_CONTENTTYPE);
        });

        // -- Handle the exceptions
        handleExceptions(jsonConverter);

        // -- Create Response Transformer
        //TODO we can make this more generic
        this.responseTransormer = (model) -> jsonConverter.toJson(model);
    }

    /**
     * Validates content type is json.
     *
     * @param req
     * @throws InvalidPayloadException
     */
    protected void validateContentType(Request req) throws InvalidPayloadException {
        if (req.contentType() == null || !req.contentType().toLowerCase().contains("application/json")) {
            throw new InvalidPayloadException("Invalid content type: " + req.contentType());
        }
    }

    /**
     * Exceptions handled by controllers.
     *
     * @param jsonConverter
     */
    protected void handleExceptions(final Gson jsonConverter) {
        exception(InvalidPayloadException.class, (ex, req, res) -> {
            res.status(HttpStatus.BAD_REQUEST_400);
            res.type(Constants.STANDARD_RESPONSE_CONTENTTYPE);
            res.body(jsonConverter.toJson(new Response(Constants.RESPONSE_ERROR, ex.getMessage())));
        });
        exception(TransferRejectedException.class, (ex, req, res) -> {
            res.status(HttpStatus.BAD_REQUEST_400);
            res.type(Constants.STANDARD_RESPONSE_CONTENTTYPE);
            res.body(jsonConverter.toJson(new Response(Constants.RESPONSE_ERROR, ex.getMessage())));
        });
        exception(NotFoundException.class, (ex, req, res) -> {
            res.status(HttpStatus.NOT_FOUND_404);
            res.type(Constants.STANDARD_RESPONSE_CONTENTTYPE);
            res.body(jsonConverter.toJson(new Response(Constants.RESPONSE_ERROR, ex.getMessage())));
        });
        exception(Exception.class, (ex, req, res) -> {
            res.status(HttpStatus.INTERNAL_SERVER_ERROR_500);
            res.type(Constants.STANDARD_RESPONSE_CONTENTTYPE);
            res.body(jsonConverter.toJson(new Response(Constants.RESPONSE_ERROR, ex.getMessage())));
        });
    }

    /**
     * Returns Response transformer.
     *
     * @return
     */
    protected ResponseTransformer getTransformer() {
        return this.responseTransormer;
    }

    public abstract void configure(String basePath);
}