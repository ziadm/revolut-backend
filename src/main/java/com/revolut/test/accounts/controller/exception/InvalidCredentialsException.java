package com.revolut.test.accounts.controller.exception;

@SuppressWarnings("serial")
public class InvalidCredentialsException extends Exception {

    public InvalidCredentialsException() {
        super();
    }

    public InvalidCredentialsException(String message) {
        super(message);
    }

}