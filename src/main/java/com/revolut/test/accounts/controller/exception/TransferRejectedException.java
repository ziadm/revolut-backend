package com.revolut.test.accounts.controller.exception;

@SuppressWarnings("serial")
public class TransferRejectedException extends Exception {
	public TransferRejectedException() {
		super();
	}

	public TransferRejectedException(String message) {
		super(message);
	}
}
