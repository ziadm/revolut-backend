package com.revolut.test.accounts.controller;

import com.google.gson.Gson;
import com.revolut.test.accounts.models.Response;
import com.revolut.test.accounts.models.Account;
import com.revolut.test.accounts.service.AccountService;
import org.eclipse.jetty.http.HttpStatus;
import spark.Request;

import java.sql.SQLException;

import static spark.Spark.*;

/**
 * Accounts Controller.
 * Exposes restful interface for create, get and getAll accounts.
 */
public class AccountsController extends BaseController {

	private final AccountService service;
	private final Gson jsonConverter;

	public AccountsController(final AccountService service, final Gson jsonConverter) {
		super(jsonConverter);
		this.service = service;
		this.jsonConverter = jsonConverter;
	}

	@Override
	public void configure(String basePath) {
		initializeController(this.service, this.jsonConverter, basePath);
	}

	private void initializeController(final AccountService service, final Gson jsonConverter, final String basePath) {
		// -- Get all entities
		get(basePath + "/accounts", (req, res) -> {
			return getAll(service);
		}, getTransformer());

		// -- Get one specific entity
		get(basePath + "/accounts/:id", (req, res) -> {
			return getOneEntity(service, req, res);
		}, getTransformer());

		// -- Add an entity
		post(basePath + "/accounts", (req, res) -> {
			validateContentType(req);

			return addEntity(service, jsonConverter, req, res);
		}, getTransformer());
	}

	/**
	 * Returns all accounts
	 *
	 * @param service
	 * @return
	 * @throws SQLException
	 */
	private Response getAll(final AccountService service) throws SQLException {
		return new Response(service.getAllAccounts());
	}

	/**
	 * Extracts id parameter from url path.
	 * Returns account with this id.
	 *
	 * @param service
	 * @param req
	 * @param res
	 * @return
	 * @throws com.revolut.test.accounts.controller.exception.NotFoundException
	 */
	private Response getOneEntity(final AccountService service, Request req, spark.Response res)
			throws Exception {
		long id = Integer.parseInt(req.params(":id"));

		return new Response(service.getAccount(id));
	}

	/**
	 * Add new Account.
	 * throws InvalidPayloadException if number or balance don't exist.
	 *
	 * //TODO should throw exception if account number is not unique.
	 * //TODO should add account currency.
	 *
	 * @param service
	 * @param jsonConverter
	 * @param req
	 * @param res
	 * @return
	 * @throws com.revolut.test.accounts.controller.exception.InvalidPayloadException
	 */
	private Response addEntity(final AccountService service, final Gson jsonConverter, Request req, spark.Response res)
			throws Exception {
		String payload = req.body();
		Account entityToAdd = jsonConverter.fromJson(payload, Account.class);

		Account newAccount = service.createNewAccount(entityToAdd);
		res.status(HttpStatus.CREATED_201);
		return new Response(newAccount);
	}
}