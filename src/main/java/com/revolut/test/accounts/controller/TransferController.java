package com.revolut.test.accounts.controller;

import com.google.gson.Gson;
import com.revolut.test.accounts.controller.exception.InvalidPayloadException;
import com.revolut.test.accounts.controller.exception.NotFoundException;
import com.revolut.test.accounts.controller.exception.TransferRejectedException;
import com.revolut.test.accounts.models.Response;
import com.revolut.test.accounts.models.Transfer;
import com.revolut.test.accounts.models.TransferRequest;
import com.revolut.test.accounts.service.TransferService;
import org.eclipse.jetty.http.HttpStatus;
import spark.Request;

import java.sql.SQLException;

import static spark.Spark.get;
import static spark.Spark.post;


/**
 * Transfers Controller.
 * Exposes restful interface for get, getAll and make a transfer.
 */
public class TransferController extends BaseController {

    private final TransferService service;
    private final Gson jsonConverter;

    public TransferController(final TransferService service, final Gson jsonConverter) {
        super(jsonConverter);
        this.service = service;
        this.jsonConverter = jsonConverter;
    }


    @Override
    public void configure(String basePath) {
        initializeController(this.service, this.jsonConverter, basePath);
    }

    public void initializeController(final TransferService service, final Gson jsonConverter, final String basePath) {
        // -- Get all entities
        get(basePath + "/transfers", (req, res) -> {
            return getAll(service);
        }, getTransformer());

        // -- Get one specific entity
        get(basePath + "/transfers/:id", (req, res) -> {
            return getOneEntity(service, req, res);
        }, getTransformer());

        // -- Add an entity
        post(basePath + "/transfers", (req, res) -> {
            validateContentType(req);

            return addEntity(service, jsonConverter, req, res);
        }, getTransformer());
    }

    /**
     * Returns transfers sorted by date.
     *
     * @param service
     * @return
     * @throws SQLException
     */
    public Response getAll(final TransferService service) throws SQLException {
        return new Response(service.getAllTransfers());
    }

    /**
     * Returns single transfer using id from request parameters.
     *
     * @param service
     * @param req
     * @param res
     * @return
     * @throws Exception
     */
    public Response getOneEntity(final TransferService service, Request req, spark.Response res) throws Exception {
        long id = Integer.parseInt(req.params(":id"));
        return new Response(service.getTransfer(id));
    }

    /**
     * Transfers money from one account to the other.
     * Moves balance between the two accounts, and creates a new transfer record in the database.
     *
     * @param service
     * @param jsonConverter
     * @param req
     * @param res
     * @return
     * @throws SQLException
     * @throws TransferRejectedException
     * @throws InvalidPayloadException
     * @throws NotFoundException
     */
    public Response addEntity(final TransferService service, final Gson jsonConverter, Request req, spark.Response res)
            throws SQLException, TransferRejectedException, InvalidPayloadException, NotFoundException {
        String payload = req.body();
        TransferRequest entityToAdd = jsonConverter.fromJson(payload, TransferRequest.class);

        Transfer newAccount = service.transfer(entityToAdd);
        res.status(HttpStatus.CREATED_201);
        return new Response(newAccount);
    }
}
