package com.revolut.test.accounts;

import com.google.gson.GsonBuilder;
import com.revolut.test.accounts.controller.AccountsController;
import com.revolut.test.accounts.controller.TransferController;
import com.revolut.test.accounts.repository.impl.H2AccountsRepo;
import com.revolut.test.accounts.repository.impl.H2TransfersRepo;
import com.revolut.test.accounts.service.impl.AccountServiceImpl;
import com.revolut.test.accounts.service.impl.TransferServiceImpl;
import com.revolut.test.accounts.util.RestContext;

public class AccountsApp {

    public static void main(String[] args) {
        RestContext context = new RestContext(8080, "");
        context.addEndpoint(new AccountsController(new AccountServiceImpl(new H2AccountsRepo()),
                new GsonBuilder().create()));
        context.addEndpoint(new TransferController(new TransferServiceImpl(new H2TransfersRepo(), new H2AccountsRepo()),
                new GsonBuilder().create()));
        context.awaitInit();
    }
}