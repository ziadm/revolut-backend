package com.revolut.test.accounts.service;

import java.sql.SQLException;
import java.util.Collection;

import com.revolut.test.accounts.models.Account;

public interface AccountService {
	Account createNewAccount(Account account) throws Exception;
	Collection<Account> getAllAccounts() throws SQLException;
	Account getAccount(Long l) throws Exception;
}
