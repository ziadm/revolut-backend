package com.revolut.test.accounts.service.impl;

import com.revolut.test.accounts.controller.exception.InvalidPayloadException;
import com.revolut.test.accounts.controller.exception.NotFoundException;
import com.revolut.test.accounts.models.Account;
import com.revolut.test.accounts.repository.AccountsRepo;
import com.revolut.test.accounts.service.AccountService;
import com.revolut.test.accounts.util.DbContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Optional;

/**
 * Business logic for the accounts.
 *
 */
public class AccountServiceImpl implements AccountService {
    private final Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);

    private final AccountsRepo accountRepo;

    public AccountServiceImpl(AccountsRepo accountRepo) {
        this.accountRepo = accountRepo;
    }

    /**
     * Using a transaction Creates and Returns a new account.
     * Account fields are validated before the account is created
     *
     * @param account
     * @return
     * @throws Exception
     */
    @Override
    public Account createNewAccount(Account account) throws Exception {
        validateAccount(account);
        return DbContext.INSTANCE.usingTransaction((Connection connection) -> {
            accountRepo.addAccount(connection, account);
            return accountRepo.getLatestAccount(connection).get();
        });
    }

    /**
     * Gets Single account from the table.
     *
     * Waits for an account if locked. Will throw an SQLException if lock time out exceeded.
     * Client should retry the request.
     *
     * In case NotFoundException client should not retry the request.
     *
     * @param id
     * @return
     * @throws NotFoundException
     * @throws SQLException
     */
    @Override
    public Account getAccount(Long id) throws NotFoundException, SQLException {
        try (Connection con = DbContext.INSTANCE.getContext()) {
            Optional<Account> optAccount = accountRepo.getAccount(con, id);
            return optAccount.orElseThrow(() -> new NotFoundException(String.format("Account [%s] not found", id)));
        }
    }

    /**
     * Returns list of accounts ordered by date.
     *
     * @return
     * @throws SQLException
     */
    @Override
    public Collection<Account> getAllAccounts() throws SQLException {
        try (Connection con = DbContext.INSTANCE.getContext()) {
            return accountRepo.getAll(con);
        }
    }

    /**
     * Helper method to validate new account.
     *
     * //TODO should we validate account balances not zero?
     *
     * @param account
     * @throws InvalidPayloadException
     */
    private void validateAccount(Account account) throws InvalidPayloadException {
        if (account == null)
            throw new InvalidPayloadException("Account Invalid");
        if (account.getBalance() == null)
            throw new InvalidPayloadException("Account balance does not exist");
        if (account.getNumber() == null)
            throw new InvalidPayloadException("Account number does not exist");
        if (account.getBalance().signum() < 0)
            throw new InvalidPayloadException("Account Balance Can't be negative");
    }
}