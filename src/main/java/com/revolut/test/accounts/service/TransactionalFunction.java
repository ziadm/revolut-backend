package com.revolut.test.accounts.service;

import com.revolut.test.accounts.controller.exception.NotFoundException;
import com.revolut.test.accounts.controller.exception.TransferRejectedException;

import java.sql.SQLException;

@FunctionalInterface
public interface TransactionalFunction<Connection, R> {
    R apply(java.sql.Connection t) throws SQLException, NotFoundException, TransferRejectedException;
}