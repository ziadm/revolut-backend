package com.revolut.test.accounts.service.impl;

import com.revolut.test.accounts.controller.exception.InvalidPayloadException;
import com.revolut.test.accounts.controller.exception.NotFoundException;
import com.revolut.test.accounts.controller.exception.TransferRejectedException;
import com.revolut.test.accounts.models.Account;
import com.revolut.test.accounts.models.Transfer;
import com.revolut.test.accounts.models.TransferRequest;
import com.revolut.test.accounts.repository.AccountsRepo;
import com.revolut.test.accounts.repository.TransferRepo;
import com.revolut.test.accounts.service.TransferService;
import com.revolut.test.accounts.util.DbContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;

/**
 * Business logic for transfers
 *
 */
public class TransferServiceImpl implements TransferService {
    private final Logger logger = LoggerFactory.getLogger(TransferServiceImpl.class);

    private TransferRepo transferRepo;
    private AccountsRepo accountRepo;

    public TransferServiceImpl(TransferRepo transferRepo, AccountsRepo accountRepo) {
        this.transferRepo = transferRepo;
        this.accountRepo = accountRepo;
    }


    /**
     * Returns single transaction using id.
     * Throws NotFoundException if transaction does not exist.
     *
     * @param id
     * @return
     * @throws NotFoundException
     * @throws SQLException
     */
    @Override
    public Transfer getTransfer(final Long id) throws NotFoundException, SQLException {
        try (Connection con = DbContext.INSTANCE.getContext()) {
            Optional<Transfer> optAccount = transferRepo.getTransfer(con, id);
            return optAccount.orElseThrow(() -> new NotFoundException(String.format("Transfer [%s] not found", id)));
        }
    }

    /**
     * Returns list of transfers sorted by date.
     *
     * @return
     * @throws SQLException
     */
    @Override
    public Collection<Transfer> getAllTransfers() throws SQLException {
        try (Connection con = DbContext.INSTANCE.getContext()) {
            return transferRepo.getAll(con);
        }
    }

    /**
     * Validates the transfer request.
     * Locks the two accounts.
     * Moves the balance between the two accounts
     * Creates a new transfer element in the transfers table.
     * Releases the lock returning the latest transfer
     *
     * @param request
     * @return
     * @throws SQLException if database exception happens, rolling back the transaction
     * @throws TransferRejectedException if from_acc does not have enough funds
     * @throws NotFoundException if any of the accounts does not exist
     * @throws InvalidPayloadException if required fields are missing or are wrong
     */
    @Override
    public Transfer transfer(TransferRequest request) throws SQLException, TransferRejectedException, NotFoundException, InvalidPayloadException {
        validateTransfer(request);
        return DbContext.INSTANCE.usingTransaction((Connection connection) -> {
            Map<Long, Account> accounts = accountRepo.getTwoAccountsForUpdate(connection, request.getFromId(), request.getToId());

            if (!accounts.containsKey(request.getFromId())) {
                throw new NotFoundException(String.format("Account [%s] not found", request.getFromId()));
            }
            if (!accounts.containsKey(request.getToId())) {
                throw new NotFoundException(String.format("Account [%s] not found", request.getToId()));
            }

            BigDecimal fromBalance = accounts.get(request.getFromId()).getBalance().subtract(request.getAmount());
            BigDecimal toBalance = accounts.get(request.getToId()).getBalance().add(request.getAmount());

            if (fromBalance.signum() < 0) {
                throw new TransferRejectedException("Balance Insufficient");
            }
            accountRepo.updateAccountBalance(connection, request.getFromId(), fromBalance);
            accountRepo.updateAccountBalance(connection, request.getToId(), toBalance);
            transferRepo.insertTransfer(connection, request);
            return transferRepo.getLatestTransfer(connection).get();
        });
    }

    /**
     * Helper method that validates transfer.
     *
     * @param transfer
     * @throws InvalidPayloadException
     */
    private void validateTransfer(TransferRequest transfer) throws InvalidPayloadException {
        if (transfer == null)
            throw new InvalidPayloadException("Transfer Invalid");
        if (transfer.getFromId() == null)
            throw new InvalidPayloadException("Transfer from id does not exist");
        if (transfer.getToId() == null)
            throw new InvalidPayloadException("Transfer to id does not exist");
        if (transfer.getAmount() == null)
            throw new InvalidPayloadException("Transfer amount does not exist");
        if (transfer.getAmount().signum() <= 0)
            throw new InvalidPayloadException("Transfer amount should be a positive non zero decimal");
    }
}
