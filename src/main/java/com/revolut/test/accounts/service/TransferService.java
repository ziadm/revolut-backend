package com.revolut.test.accounts.service;

import com.revolut.test.accounts.controller.exception.InvalidPayloadException;
import com.revolut.test.accounts.controller.exception.NotFoundException;
import com.revolut.test.accounts.controller.exception.TransferRejectedException;
import com.revolut.test.accounts.models.Transfer;
import com.revolut.test.accounts.models.TransferRequest;

import java.sql.SQLException;
import java.util.Collection;

public interface TransferService {
    Transfer getTransfer(Long id) throws NotFoundException, SQLException;
    Transfer transfer(TransferRequest request) throws SQLException, TransferRejectedException, NotFoundException, InvalidPayloadException;
	Collection<Transfer> getAllTransfers() throws SQLException;
}
