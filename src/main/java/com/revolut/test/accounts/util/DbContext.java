package com.revolut.test.accounts.util;

import com.revolut.test.accounts.controller.exception.NotFoundException;
import com.revolut.test.accounts.controller.exception.TransferRejectedException;
import com.revolut.test.accounts.service.TransactionalFunction;
import com.typesafe.config.ConfigFactory;
import org.h2.jdbcx.JdbcConnectionPool;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Singleton db context.
 * <p>
 * //TODO configuration should be managed better if more requirements about deployment env are given.
 */
public enum DbContext {

    INSTANCE;

    JdbcConnectionPool cp;
    //HikariDataSource cp;

    DbContext() {
        //HikariConfig config = new HikariConfig();
        //config.setJdbcUrl(getUrl());
        //config.setUsername("sa");
        //config.setPassword("sa");
        //p = new HikariDataSource(config);
        cp = JdbcConnectionPool.create(getUrl(), "sa", "");
        //NOTE: its very important to keep the connection limit to 1 when using embedded h2.
        //DUE to a bug in h2, we can't use more than one connection.
        cp.setMaxConnections(1);
    }

    private String getUrl() {
        return ConfigFactory.load().getString("datasource.url");
    }

    public Connection getContext() throws SQLException {
        return cp.getConnection();
    }

    /**
     * Wrapping an sql function around a transaction. Rolling back when issues happen.
     *
     * @param f
     * @param <B>
     * @return
     * @throws SQLException
     */
    public <B> B usingTransaction(TransactionalFunction<Connection, B> f)
            throws SQLException, NotFoundException, TransferRejectedException {
        boolean initialAutocommit = false;
        Connection connection = null;
        try {
            connection = getContext();
            initialAutocommit = connection.getAutoCommit();
            connection.setAutoCommit(false);
            B returnValue = f.apply(connection);
            connection.commit();
            return returnValue;
        } catch (Throwable throwable) {
            if (connection != null) {
                connection.rollback();
            }
            throw throwable;
        } finally {
            if (connection != null) {
                if (initialAutocommit) {
                    connection.setAutoCommit(true);
                }
                connection.close();
            }
        }
    }
}
