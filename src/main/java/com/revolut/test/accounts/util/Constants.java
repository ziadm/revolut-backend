package com.revolut.test.accounts.util;

/**
 * Constant values.
 *
 */
public class Constants {

    public static int RESPONSE_OK = 0;
    public static int RESPONSE_ERROR = -1;

    public static String STANDARD_RESPONSE_CONTENTTYPE = "application/json;charset=UTF-8";
}