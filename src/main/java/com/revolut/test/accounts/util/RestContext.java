package com.revolut.test.accounts.util;


import com.revolut.test.accounts.controller.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Spark;

/**
 * Context that will manage the server instance.
 * We can add as many controllers using this class.
 *
 */
public class RestContext {

    private static final Logger logger = LoggerFactory.getLogger(RestContext.class);

    private final String basePath;

    public RestContext(int port, String basePath) {
        this.basePath = basePath;
        Spark.port(port);
    }

    public void addEndpoint(BaseController endpoint) {
        endpoint.configure(basePath);
        logger.info("REST endpoints registered for {}.", endpoint.getClass().getSimpleName());
    }

    public void awaitInit() {
        Spark.awaitInitialization();
    }

    public void stop() {
        Spark.stop();
    }
}
