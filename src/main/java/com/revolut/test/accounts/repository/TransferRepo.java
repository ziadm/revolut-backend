package com.revolut.test.accounts.repository;

import com.revolut.test.accounts.models.Transfer;
import com.revolut.test.accounts.models.TransferRequest;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Optional;

public interface TransferRepo {

    void insertTransfer(Connection context, TransferRequest request) throws SQLException;
    Collection<Transfer> getAll(Connection context) throws SQLException;
    Optional<Transfer> getTransfer(Connection context, Long id) throws SQLException;
    Optional<Transfer> getLatestTransfer(Connection con) throws SQLException;
}
