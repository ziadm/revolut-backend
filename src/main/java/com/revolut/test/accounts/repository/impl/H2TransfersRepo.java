package com.revolut.test.accounts.repository.impl;

import com.revolut.test.accounts.models.Account;
import com.revolut.test.accounts.models.Transfer;
import com.revolut.test.accounts.models.TransferRequest;
import com.revolut.test.accounts.repository.TransferRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public class H2TransfersRepo implements TransferRepo {

    private final Logger logger = LoggerFactory.getLogger(H2TransfersRepo.class);

    private final static String GET_ALL_TRANSFERS = "SELECT TRANSFER.*, FROM_ACCOUNT.ID \"FROM_ACCOUNT_ID\", FROM_ACCOUNT.NUMBER \"FROM_ACCOUNT_NUMBER\", TO_ACCOUNT.ID \"TO_ACCOUNT_ID\", TO_ACCOUNT.NUMBER \"TO_ACCOUNT_NUMBER\" FROM TRANSFER AS TRANSFER JOIN ACCOUNT AS FROM_ACCOUNT ON TRANSFER.FROM_ACC = FROM_ACCOUNT.ID JOIN ACCOUNT AS TO_ACCOUNT ON TRANSFER.TO_ACC = TO_ACCOUNT.ID ORDER BY DATE DESC ";
    private final static String INSERT_TRANSFER = "INSERT INTO TRANSFER  (AMOUNT, FROM_ACC, TO_ACC, DATE) VALUES (?, ?, ?, NOW()) ";
    private final static String GET_TRANSFER = "SELECT TRANSFER.*, FROM_ACCOUNT.ID \"FROM_ACCOUNT_ID\", FROM_ACCOUNT.NUMBER \"FROM_ACCOUNT_NUMBER\", TO_ACCOUNT.ID \"TO_ACCOUNT_ID\", TO_ACCOUNT.NUMBER \"TO_ACCOUNT_NUMBER\" FROM TRANSFER AS TRANSFER JOIN ACCOUNT AS FROM_ACCOUNT ON TRANSFER.FROM_ACC = FROM_ACCOUNT.ID JOIN ACCOUNT AS TO_ACCOUNT ON TRANSFER.TO_ACC = TO_ACCOUNT.ID WHERE TRANSFER.ID = ";
    private final static String GET_NEWEST_TRANSFER = "SELECT TRANSFER.*, FROM_ACCOUNT.ID \"FROM_ACCOUNT_ID\", FROM_ACCOUNT.NUMBER \"FROM_ACCOUNT_NUMBER\", TO_ACCOUNT.ID \"TO_ACCOUNT_ID\", TO_ACCOUNT.NUMBER \"TO_ACCOUNT_NUMBER\" FROM TRANSFER AS TRANSFER JOIN ACCOUNT AS FROM_ACCOUNT ON TRANSFER.FROM_ACC = FROM_ACCOUNT.ID JOIN ACCOUNT AS TO_ACCOUNT ON TRANSFER.TO_ACC = TO_ACCOUNT.ID WHERE TRANSFER.ID = (SELECT LAST_INSERT_ID())";

    public H2TransfersRepo() {

    }

    /**
     * Returns all transfers sorted by date.
     * //TODO add pagination
     *
     * @param context
     * @return
     * @throws SQLException
     */
    public Collection<Transfer> getAll(Connection context) throws SQLException {
        List<Transfer> transfers = new ArrayList<>();
        try (Statement st = context.createStatement();
             ResultSet rs = st.executeQuery(GET_ALL_TRANSFERS)) {
            while (rs.next()) {
                transfers.add(mapTransfers(rs));
            }
            return transfers;
        } catch (SQLException ex) {
            logger.warn("Error getting all transfers", ex);
            throw ex;
        }
    }

    /**
     * Returns last transfer inserted in the table.
     *
     * @param con
     * @return
     * @throws SQLException
     */
    @Override
    public Optional<Transfer> getLatestTransfer(Connection con) throws SQLException {
        try (
                PreparedStatement st = con.prepareStatement(GET_NEWEST_TRANSFER);
                ResultSet rs = st.executeQuery()) {
            return rs.next() ? Optional.of(mapTransfers(rs)) : Optional.empty();
        } catch (SQLException ex) {
            logger.warn("Error getting latest transfer", ex);
            throw ex;
        }
    }

    /**
     * Returns optional of transfer using transfer id.
     *
     * @param context
     * @param id
     * @return
     * @throws SQLException
     */
    @Override
    public Optional<Transfer> getTransfer(Connection context, Long id) throws SQLException {
        try (PreparedStatement st = context.prepareStatement(GET_TRANSFER + id);
             ResultSet rs = st.executeQuery()) {
            return rs.next() ? Optional.of(mapTransfers(rs)) : Optional.empty();
        } catch (SQLException ex) {
            logger.warn("Error getting transfer with id: " + id, ex);
            throw ex;
        }
    }


    /**
     * Creates a new Transfer.
     *
     * @param context
     * @param request
     * @throws SQLException
     */
    @Override
    public void insertTransfer(Connection context, TransferRequest request) throws SQLException {
        try (PreparedStatement st = context.prepareStatement(INSERT_TRANSFER)) {
            st.setBigDecimal(1, request.getAmount());
            st.setLong(2, request.getFromId());
            st.setLong(3, request.getToId());
            int rs = st.executeUpdate();
            logger.trace("Inserting new transfer affected {} records", rs);
        } catch (SQLException ex) {
            logger.warn("Error inserting new transfer", ex);
            throw ex;
        }
    }

    /**
     * Helper method that maps result sets into Value Objects.
     *
     * @param rs
     * @return
     * @throws SQLException
     */
    private Transfer mapTransfers(ResultSet rs) throws SQLException {
        Transfer t = new Transfer();
        t.setId(rs.getLong("ID"));
        t.setTimestamp(rs.getTimestamp("DATE"));
        t.setAmount(rs.getBigDecimal("AMOUNT"));
        t.setFromAcc(new Account(rs.getLong("FROM_ACCOUNT_ID"), rs.getString("FROM_ACCOUNT_NUMBER"), null));
        t.setToAcc(new Account(rs.getLong("TO_ACCOUNT_ID"), rs.getString("TO_ACCOUNT_NUMBER"), null));
        return t;
    }
}
