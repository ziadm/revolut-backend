package com.revolut.test.accounts.repository;

import com.revolut.test.accounts.models.Account;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;

public interface AccountsRepo {
    void addAccount(Connection context, Account user) throws SQLException;
    Collection<Account> getAll(Connection context) throws SQLException;
    Optional<Account> getAccount(Connection context, Long id) throws SQLException;
    int updateAccountBalance(Connection context, Long fromId, BigDecimal fromBalance) throws SQLException;
    Optional<Account> getLatestAccount(Connection con) throws SQLException;
    Map<Long, Account> getTwoAccountsForUpdate(Connection con, Long firstId, Long secondId) throws SQLException;
}
