package com.revolut.test.accounts.repository.impl;

import com.revolut.test.accounts.models.Account;
import com.revolut.test.accounts.repository.AccountsRepo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.sql.*;
import java.util.*;

/**
 *
 */
public class H2AccountsRepo implements AccountsRepo {

    private final Logger logger = LoggerFactory.getLogger(H2AccountsRepo.class);

    private final static String UPDATE_ACCOUNT_BALANCE = "UPDATE ACCOUNT SET BALANCE = ?, UPDATEDAT = NOW() WHERE ID = ?";
    private final static String GET_ALL_ACCOUNTS = "SELECT * FROM ACCOUNT";
    private final static String GET_ACCOUNT = "SELECT * FROM ACCOUNT WHERE ID = ";
    private final static String GET_ACCOUNTS_FOR_UPDATE = "SELECT * FROM ACCOUNT WHERE ID = ? OR ID = ? FOR UPDATE";
    private final static String INSERT_ACCOUNT = "INSERT INTO ACCOUNT (NUMBER, BALANCE, CREATEDAT, UPDATEDAT) VALUES (?, ?, NOW(), NOW())";
    private final static String GET_NEWEST_ACCOUNT = "SELECT * FROM ACCOUNT WHERE ID = (SELECT LAST_INSERT_ID())";

    public H2AccountsRepo() {

    }

    /**
     * Add a new account to the database.
     * Account ids are auto incremented in the database.
     *
     * @param con
     * @param account
     * @throws SQLException
     */
    @Override
    public void addAccount(Connection con, Account account) throws SQLException {
        try (
                PreparedStatement st = con.prepareStatement(INSERT_ACCOUNT)) {
            st.setString(1, account.getNumber());
            st.setBigDecimal(2, account.getBalance());
            int id = st.executeUpdate();
            logger.trace("Add account query affected: {} records", id);
        } catch (SQLException ex) {
            logger.warn("Error adding account", ex);
            throw ex;
        }
    }

    /**
     * Returns list of accounts.
     *
     * //TODO add pagination
     *
     * @param con
     * @return
     * @throws SQLException
     */
    @Override
    public Collection<Account> getAll(Connection con) throws SQLException {
        List<Account> accounts = new ArrayList<>();
        try (
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery(GET_ALL_ACCOUNTS)) {
            while (rs.next()) {
                accounts.add(mapAccount(rs));
            }
        } catch (SQLException ex) {
            logger.warn("Error getting all accounts", ex);
            throw ex;
        }
        return accounts;
    }

    /**
     * Returns latest account created.
     *
     * @param con
     * @return
     * @throws SQLException
     */
    @Override
    public Optional<Account> getLatestAccount(Connection con) throws SQLException {
        try (
                PreparedStatement st = con.prepareStatement(GET_NEWEST_ACCOUNT);
                ResultSet rs = st.executeQuery()) {
            return rs.next() ? Optional.of(mapAccount(rs)) : Optional.empty();
        } catch (SQLException ex) {
            logger.warn("Error getting latest account", ex);
            throw ex;
        }
    }

    /**
     * Returns optional holding an account using the id
     *
     * @param con
     * @param id
     * @return
     * @throws SQLException
     */
    @Override
    public Optional<Account> getAccount(Connection con, Long id) throws SQLException {
        try (
                Statement st = con.createStatement();
                ResultSet rs = st.executeQuery(GET_ACCOUNT + id)) {
            return rs.next() ? Optional.of(mapAccount(rs)) : Optional.empty();
        } catch (SQLException ex) {
            logger.warn("Error getting account with id: " + id, ex);
            throw ex;
        }
    }

    /**
     * Selects two accounts for update.
     * Make sure to commit this connection or else these accounts will remain locked.
     *
     * @param con
     * @param firstId
     * @param secondId
     * @return
     * @throws SQLException
     */
    @Override
    public Map<Long, Account> getTwoAccountsForUpdate(Connection con, Long firstId, Long secondId) throws SQLException {
        try (
                PreparedStatement st = con.prepareStatement(GET_ACCOUNTS_FOR_UPDATE)) {
            st.setLong(1, firstId);
            st.setLong(2, secondId);
            ResultSet rs = st.executeQuery();
            Map<Long, Account> accounts = new HashMap<>();
            while (rs.next()) {
                Account account = mapAccount(rs);
                accounts.put(account.getId(), account);
            }
            return accounts;
        } catch (SQLException ex) {
            logger.warn("Error getting accounts ", ex);
            throw ex;
        }
    }

    /**
     * Update account balance using its id.
     *
     * @param con
     * @param id
     * @param balance
     * @return
     * @throws SQLException
     */
    public int updateAccountBalance(Connection con, Long id, BigDecimal balance) throws SQLException {
        try (
                PreparedStatement st = con.prepareStatement(UPDATE_ACCOUNT_BALANCE)) {
            st.setBigDecimal(1, balance);
            st.setLong(2, id);
            return st.executeUpdate();
        } catch (SQLException ex) {
            logger.warn("Error updating account balance", ex);
            throw ex;
        }
    }

    /**
     * Helper method that maps result sets into Value Objects.
     *
     * @param rs
     * @return
     * @throws SQLException
     */
    private Account mapAccount(ResultSet rs) throws SQLException {
        logger.trace("Mapping Account");
        Account acc = new Account();
        acc.setId(rs.getLong("ID"));
        acc.setNumber(rs.getString("NUMBER"));
        acc.setBalance(rs.getBigDecimal("BALANCE"));
        acc.setCreatedAt(rs.getTimestamp("CREATEDAT"));
        acc.setUpdatedAt(rs.getTimestamp("UPDATEDAT"));
        return acc;
    }
}
