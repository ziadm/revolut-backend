FROM maven:3.5-jdk-8 AS build
COPY src /usr/src/app/src
COPY pom.xml /usr/src/app
RUN mvn -f /usr/src/app/pom.xml clean package

FROM gcr.io/distroless/java
COPY --from=build /usr/src/app/target/accounts-service-0.0.1-SNAPSHOT-jar-with-dependencies.jar /usr/app/accounts-service-0.0.1-SNAPSHOT-jar-with-dependencies.jar
EXPOSE 8080
ENTRYPOINT ["java","-cp","/usr/app/accounts-service-0.0.1-SNAPSHOT-jar-with-dependencies.jar", "com.revolut.test.accounts.AccountsApp"]
